from flask import Flask
from flask_pymongo import PyMongo
from flask_cors import CORS

app = Flask(__name__)
#app.config['MONGO_URI']='mongodb://localhost/mrf9'
#mongoConnect = PyMongo(app)

#db = mongoConnect.db.users

@app.route('/passwords', methods=['GET'])
def getpass():
    return '<h1> Hola a todos </h1>'

@app.route('/password/<id>', methods=['GET'])
def getpassUnique():
    return '<h1> Hola a todos </h1>'

@app.route('/create', methods=['POST'])
def createpass():
    return '<h1> Hola a todos </h1>'

@app.route('/modify/<id>', methods=['POST'])
def modifypass():
    return '<h1> Hola a todos </h1>'

@app.route('/droppass/<id>', methods=['POST'])
def droppass():
    return '<h1> Hola a todos </h1>'

if __name__ == "__main__":
    app.run(debug=True)